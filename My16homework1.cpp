﻿#include <iostream>
#include <string>



class Animal 
{
public:
    std::string name;
    virtual void Voice() // Виртуальная функция 
    {
        std::cout << "Not voice" << '\n';
    }
};

class Dog : public Animal 
{
public:
    void Voice() override // Переопределение метода
    {
        std::cout << "Woof!" << '\n';
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!" << '\n';
    }
};


class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo!" << '\n';
    }
};


int main()
{
    Animal* Animals[3]; 

    Dog* dog = new Dog();
    Cat* cat = new Cat();
    Cow* cow = new Cow();
    Animal* animal = new Animal();

    Animals[0] = dog;
    Animals[1] = cat;
    Animals[2] = cow;

    for (int i = 0; i < 3; i++)
    {
       Animals[i]->Voice();
    }
}
